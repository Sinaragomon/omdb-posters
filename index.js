function ready(fn) {
    if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading"){
      fn();
    } else {
      document.addEventListener('DOMContentLoaded', fn);
    }
}

ready(() => {
    // const OMDB_API_KEY = '338f9a63';
    const OMDB_API_KEY = 'fc68bf55';

    let searchField = document.querySelector('input[name="search"]'); // get search field
    let moviesList = document.querySelector('.movies-list'); // get movies list DOM element
    let moviesGetStatusEl = document.querySelector('.movies__search-status'); // get movies search status DOM element
    let searchForm = document.getElementById('searchForm'); // get search form
    let searchBtn = document.getElementById('searchBtn'); // get search btn
    let cloneMovie = document.getElementById('cloneMovie'); // get movie DOM for clone
    let infoModal = document.getElementById('movieModal'); // get movie info modal DOM
    let infoModalBox = infoModal.querySelector('.box');
    let xhr = new XMLHttpRequest();
    let moreInfoXhr = new XMLHttpRequest();
    let totalMovies = 0;
    let lastSearch = '';
    let clearMovieList = true;
    let processScroll = true;
    let page = 1;
    let maxPages = 1;

    function requestMovies(){
        let val = searchField.value; // search field value

        // Additional to browser test - check search field on empty
        if(val.length === 0){
            alert('Fill search field!');
            return;
        }

        if(lastSearch === val){
            clearMovieList = false;
            page++;
        }else{
            lastSearch = val;
            clearMovieList = true;
            totalMovies = 0;
            page = 1;
            maxPages = 1;
        }        

        if(page > maxPages){
            page--;
            return;
        }

        xhr.open('GET', `http://www.omdbapi.com/?apikey=${OMDB_API_KEY}&s=${val}&page=${page}`, true);
        xhr.send();
    }

    // Handle request
    xhr.onreadystatechange = () => { 
        if (xhr.readyState != 4) return; // if request not finished - return
      
        if (xhr.status != 200) {
            // Handle error
            console.error('Error: ' + (xhr.status ? xhr.statusText : 'request failed'));
        } else {
            // Handle data
            let data = JSON.parse(xhr.responseText);
            let i=0;
            let len = 0;
            let el = null;
            let _node = null;

            // console.log(data);

            if(data.Response === 'True'){
                if(!data.hasOwnProperty('Search')){
                    moviesGetStatusEl.innerHTML = 'No Search field in data object';
                    return;
                }

                len = data.Search.length;
                totalMovies = data.totalResults ? data.totalResults : len;
                maxPages = Math.round(totalMovies / 10);
                moviesGetStatusEl.innerHTML = ''; // clear DOM get status element

                if(clearMovieList === true){
                    moviesList.innerHTML = ''; // clear DOM list
                }

                // Fill DOM list
                for(i; i<len; i++){
                    el = data.Search[i];
                    _node = cloneMovie.cloneNode(true);
                    _node.removeAttribute('id');
                    _node.setAttribute('data-imdb-id', el.imdbID);
                    if(el.Poster !== 'N/A'){
                        _node.querySelector('img').setAttribute('src', el.Poster);
                    }                    
                    _node.querySelector('.movie__title').innerHTML = el.Title;
                    moviesList.appendChild(_node);
                }

                // Off scroll block
                processScroll = true;
            }else{
                moviesGetStatusEl.innerHTML = data.Error;
            }            
        }
    }

    moreInfoXhr.onreadystatechange = () => { 
        if (moreInfoXhr.readyState != 4) return; // if request not finished - return
      
        if (moreInfoXhr.status != 200) {
            // Handle error
            console.error('Error: ' + (moreInfoXhr.status ? moreInfoXhr.statusText : 'request movie info failed'));
        } else {
            // Handle data
            let data = JSON.parse(moreInfoXhr.responseText);
            let html = '<ul class="modal-info-list">';

            console.log(data);

            if(data.Response === 'True'){
                // moviesList.innerHTML = ''; // clear DOM list
                // moviesGetStatusEl.innerHTML = ''; // clear DOM get status element

                // Fill DOM list
                for (const key in data) {
                    let value = data[key];
                    
                    // check for properties from prototype chain
                    if ( data.hasOwnProperty(key) && value !== 'N/A' ) {
                        // not a property from prototype chain    

                        html += '<li>';     
                        
                        // fill content by special key
                        switch(key){
                            case 'Response':
                            case 'Ratings':
                            break;
                            case 'Poster':
                                html = `<img src="${value}" width="170" height="250">` + html;                        
                            break;
                            case 'Website':
                                html += `<span class="title">${key}: </span>`;
                                html += `<span class="info"><a href="${value}" target="_blank">${value}</a></span>`;
                            break;
                            default:
                                html += `<span class="title">${key}: </span>`;
                                html += `<span class="info">${value}</span>`;
                            break;
                        }

                        html += '</li>';
                    }
                }

                html += '</ul>';

                infoModalBox.innerHTML = html; // update html in modal
                infoModal.classList.add('active'); // show modal
            }else{
                console.warn(data.Error);
            }            
        }
    }

    // Handle click on search field
    searchField.addEventListener('click', () => {
        searchField.select(); // select text in field
    });

    // Handle search form submit
    searchForm.addEventListener('submit', event => {
        event.preventDefault();
        requestMovies();
    });

    // Trigger first request
    searchBtn.dispatchEvent(new MouseEvent('click'));

    // Handle click on movie
    moviesList.addEventListener('click', event => {
        let el = event.target;
        let imdbID = null;

        // if we click on movie
        if(el.classList.contains('movie__click-zone')){
            imdbID = el.parentNode.getAttribute('data-imdb-id');

            moreInfoXhr.open('GET', `http://www.omdbapi.com/?apikey=${OMDB_API_KEY}&i=${imdbID}`, true);
            moreInfoXhr.send();
        }
    });

    // Close modal
    function closeMovieInfoModal(){
        infoModal.classList.remove('active');
    }
    document.querySelector('.close-modal').addEventListener('click', closeMovieInfoModal);

    document.addEventListener('keyup', event => {
        if(event.keyCode === 27){
            // if press ESC
            closeMovieInfoModal();
        }
    });

    // Load on scroll
    window.addEventListener('scroll', () => {
        if (processScroll && window.scrollY > document.scrollingElement.scrollHeight - window.innerHeight - 600) {
            processScroll = false;
            requestMovies();
        }
    });
});